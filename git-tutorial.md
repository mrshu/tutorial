
Quick and Dirty Git Tutorial
============================


Intro
-----

This tutorial aims to give you the basic knowledge of GIT as a version control
system. Therefore it omits everything the author considers unnecessary for fast
jump into development.


Setting up a BitBucket account
------------------------------

Since this tutorial will not include images you should handle the registation
process at bitbucket.org yourself. This can be done by visiting the
[registration](https://bitbucket.org/account/signup/?plan=5_users) link. 

Just note that at the time of writing this tutorial you can have unlimited
number of private git repositories at BitBucket which makes is the best at the
market for small personal/private projects.

Setting up a repro
------------------

If you want to create a new repository at BitBucket, you can do so at
[](https://bitbucket.org/repo/create). In case you do not want to create a new
repository, you might want to fork some other project. You can do this by
clicking at the `fork` button at the project page. You can try this on the
[project page](https://bitbucket.org/mrshu/tutorial/overview) of this tutorial.


Cloning the repro
-----------------

Once you have created (or forked) a repository you probably also want to create
an offline copy. This way you can work on your project offline and then commit
your changes on the server.

To clone a project use `git clone` command. For this tutorial it will be

    $ git clone git@bitbucket.org:mrshu/tutorial.git




